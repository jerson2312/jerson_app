# jerson_app

Clone the project and go to the folder then execute the following commands:

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

Open this link:
http://localhost:8080/

And use the application
