import firebase from "firebase";

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyBmc04KAZxk2SJVMSPSogW9Bx9xasxGRTA",
  authDomain: "jersonapp-7a2ee.firebaseapp.com",
  databaseURL: "https://jersonapp-7a2ee.firebaseio.com",
  projectId: "jersonapp-7a2ee",
  storageBucket: "jersonapp-7a2ee.appspot.com",
  messagingSenderId: "794298907615",
  appId: "1:794298907615:web:4fd10d620645f7657fe26a"
};

// Initialize Firebase
export default firebase.initializeApp(firebaseConfig);